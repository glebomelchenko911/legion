#include "crow.h"
#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/exception.h>
#include <thread>
#include <mutex>
#include <unordered_map>

using namespace std;

class Domino {
public:
    Domino(int leftDots, int rightDots) : leftDots_(leftDots), rightDots_(rightDots) {}
    int getLeftDots() const { return leftDots_; }
    int getRightDots() const { return rightDots_; }
    Domino turnover() const { return Domino(rightDots_, leftDots_); }

private:
    int leftDots_;
    int rightDots_;
};

bool operator<(const Domino &domino1, const Domino &domino2) {
    if (domino1.getLeftDots() != domino2.getLeftDots()) {
        return domino1.getLeftDots() < domino2.getLeftDots();
    }
    return domino1.getRightDots() < domino2.getRightDots();
}

using DominoChain = vector<Domino>;

bool canConnect(const Domino &domino1, const Domino &domino2) {
    return domino1.getRightDots() == domino2.getLeftDots();
}

void findAllDominoChains(const vector<Domino> &remainingDominos, DominoChain currentChain,
                         set<DominoChain> &allChains, mutex &chainsMutex) {
    if (remainingDominos.empty()) {
        lock_guard<mutex> guard(chainsMutex);
        allChains.insert(currentChain);
        return;
    }

    for (size_t i = 0; i < remainingDominos.size(); i++) {
        Domino currentDomino = remainingDominos[i];
        DominoChain newChain = currentChain;

        if (newChain.empty() || canConnect(newChain.back(), currentDomino)) {
            newChain.push_back(currentDomino);
            vector<Domino> newRemainingDominos = remainingDominos;
            newRemainingDominos.erase(newRemainingDominos.begin() + i);
            findAllDominoChains(newRemainingDominos, newChain, allChains, chainsMutex);
        }

        currentDomino = currentDomino.turnover();
        if (newChain.empty() || canConnect(newChain.back(), currentDomino)) {
            newChain.push_back(currentDomino);
            vector<Domino> newRemainingDominos = remainingDominos;
            newRemainingDominos.erase(newRemainingDominos.begin() + i);
            findAllDominoChains(newRemainingDominos, newChain, allChains, chainsMutex);
        }
    }
}

vector<Domino> fetchDominosFromDatabase() {
    vector<Domino> dominos;
    try {
        sql::mysql::MySQL_Driver* driver = sql::mysql::get_mysql_driver_instance();
        unique_ptr<sql::Connection> con(driver->connect("tcp://127.0.0.1:3306", "user", "password"));
        unique_ptr<sql::Statement> stmt(con->createStatement());
        stmt->execute("USE your_database");
        unique_ptr<sql::ResultSet> res(stmt->executeQuery("SELECT left_dots, right_dots FROM dominos"));

        while (res->next()) {
            int leftDots = res->getInt("left_dots");
            int rightDots = res->getInt("right_dots");
            dominos.emplace_back(leftDots, rightDots);
        }
    } catch (sql::SQLException& e) {
        cerr << "SQLException: " << e.what() << endl;
    }
    return dominos;
}

void processDominoChains(vector<Domino> dominos, set<DominoChain> &allChains, mutex &chainsMutex) {
    findAllDominoChains(dominos, DominoChain(), allChains, chainsMutex);
}

// Mock user data for authentication
unordered_map<string, string> users = { {"user1", "password1"}, {"user2", "password2"} };

bool authenticate(const string &username, const string &password) {
    auto it = users.find(username);
    return it != users.end() && it->second == password;
}

int main() {
    crow::SimpleApp app;

    CROW_ROUTE(app, "/login").methods("POST"_method)
    ([](const crow::request& req){
        auto x = crow::json::load(req.body);
        if (!x)
            return crow::response(400);
        
        string username = x["username"].s();
        string password = x["password"].s();
        
        if (authenticate(username, password)) {
            return crow::response(200, "Login successful");
        } else {
            return crow::response(401, "Unauthorized");
        }
    });

    CROW_ROUTE(app, "/dominoes")
    ([]{
        vector<Domino> dominos = fetchDominosFromDatabase();
        crow::json::wvalue x;
        for (const auto& domino : dominos) {
            x["dominos"].push_back({{"leftDots", domino.getLeftDots()}, {"rightDots", domino.getRightDots()}});
        }
        return crow::response(x);
    });

    app.port(18080).multithreaded().run();
}
