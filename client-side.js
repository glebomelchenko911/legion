document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    fetch('/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ username: username, password: password })
    })
    .then(response => {
        if (response.ok) {
            document.querySelector('.login-container').style.display = 'none';
            document.querySelector('.data-container').style.display = 'block';
            fetchDominoData();
        } else {
            return response.text().then(text => { throw new Error(text) });
        }
    })
    .catch(error => {
        document.getElementById('loginMessage').textContent = error.message;
    });
});

function fetchDominoData() {
    fetch('/dominoes')
        .then(response => response.json())
        .then(data => {
            const dominoDataDiv = document.getElementById('dominoData');
            data.dominos.forEach(domino => {
                const div = document.createElement('div');
                div.textContent = `Left Dots: ${domino.leftDots}, Right Dots: ${domino.rightDots}`;
                dominoDataDiv.appendChild(div);
            });
        })
        .catch(error => {
            console.error('Error fetching domino data:', error);
        });
}
