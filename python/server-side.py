from flask import Flask, request, jsonify
import pymysql.cursors
from typing import List, Tuple
from threading import Thread, Lock

app = Flask(__name__)

# Mock user data for authentication
users = {"user1": "password1", "user2": "password2"}

def authenticate(username: str, password: str) -> bool:
    return username in users and users[username] == password

class Domino:
    def __init__(self, left_dots: int, right_dots: int):
        self.left_dots = left_dots
        self.right_dots = right_dots

    def turnover(self) -> 'Domino':
        return Domino(self.right_dots, self.left_dots)

    def __lt__(self, other: 'Domino') -> bool:
        if self.left_dots != other.left_dots:
            return self.left_dots < other.left_dots
        return self.right_dots < other.right_dots

def can_connect(domino1: Domino, domino2: Domino) -> bool:
    return domino1.right_dots == domino2.left_dots

def find_all_domino_chains(remaining_dominos: List[Domino], current_chain: List[Domino],
                           all_chains: set, chains_lock: Lock) -> None:
    if not remaining_dominos:
        with chains_lock:
            all_chains.add(tuple(current_chain))
        return

    for i, current_domino in enumerate(remaining_dominos):
        new_chain = current_chain[:]

        if not new_chain or can_connect(new_chain[-1], current_domino):
            new_chain.append(current_domino)
            new_remaining_dominos = remaining_dominos[:i] + remaining_dominos[i+1:]
            find_all_domino_chains(new_remaining_dominos, new_chain, all_chains, chains_lock)

        current_domino = current_domino.turnover()
        if not new_chain or can_connect(new_chain[-1], current_domino):
            new_chain.append(current_domino)
            new_remaining_dominos = remaining_dominos[:i] + remaining_dominos[i+1:]
            find_all_domino_chains(new_remaining_dominos, new_chain, all_chains, chains_lock)

def fetch_dominos_from_database() -> List[Domino]:
    dominos = []
    try:
        connection = pymysql.connect(host='127.0.0.1',
                                     user='user',
                                     password='password',
                                     database='your_database',
                                     cursorclass=pymysql.cursors.DictCursor)
        with connection.cursor() as cursor:
            cursor.execute("SELECT left_dots, right_dots FROM dominos")
            for row in cursor.fetchall():
                left_dots = row['left_dots']
                right_dots = row['right_dots']
                dominos.append(Domino(left_dots, right_dots))
    except pymysql.Error as e:
        print(f"Error connecting to MySQL: {e}")
    return dominos

def process_domino_chains(dominos: List[Domino], all_chains: set, chains_lock: Lock) -> None:
    find_all_domino_chains(dominos, [], all_chains, chains_lock)

@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    if not data:
        return jsonify({"error": "Invalid request"}), 400

    username = data.get('username')
    password = data.get('password')

    if authenticate(username, password):
        return jsonify({"message": "Login successful"}), 200
    else:
        return jsonify({"error": "Unauthorized"}), 401

@app.route('/dominoes', methods=['GET'])
def get_dominoes():
    dominos = fetch_dominos_from_database()
    domino_data = [{'leftDots': domino.left_dots, 'rightDots': domino.right_dots} for domino in dominos]
    return jsonify({'dominos': domino_data})

if __name__ == '__main__':
    all_chains = set()
    chains_lock = Lock()
    dominos = fetch_dominos_from_database()
    thread = Thread(target=process_domino_chains, args=(dominos, all_chains, chains_lock))
    thread.start()
    app.run(port=18080)